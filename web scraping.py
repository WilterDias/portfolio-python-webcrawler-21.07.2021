# python -m pip install requests
# python -m pip install beautifulsoup4
import sys
import requests
from bs4 import BeautifulSoup, Tag

# Usado Python 3.9

# O resultado desse programa é um arquivo CSV que pode ser aberto no LibreOffice ou similar.
# IMPORTANTE: O encoding de leitura deve ser UTF-8.

# O programa obtém os 10 piores filmes do site IMDB (The Internet Movie Database) bastanto modificar a variável
# a seguir para 'True'. Foi verificado que os piores filmes não estão nem no Top 100, analisado por vistoria manual.
# Caso queira ver os 10 piores filmes do top 100 do site IMDB (The Internet Movie Database) modifique a variável
# abaixo para 'False'.

obter_pior_verdadeiro = True
if (len(sys.argv) == 2 and len(sys.argv[1]) != 0):
    obter_pior_verdadeiro = sys.argv[1]


def obtem_html(url: str):
    html = requests.get(url, headers = {"Accept-Language": "en-US,en;q=0.5"})
    if (html.status_code != 200):
        print('Erro na obtenção do HTML do site.')
    return BeautifulSoup(html.content, 'html.parser')


class Filme:
    def __init__(self, item_filme_html: Tag, url_base: str):
        # O valor de estrelas na página onde lista os filmes não é o mesmo valor na página do filme. Certamente é
        # inconsistência deles.
        if obter_pior_verdadeiro:
            self.url = url_base + item_filme_html.select_one('div[class = "lister-item-content"]>h3').a['href'] #contém a URL do filme.
            self.nota = item_filme_html.select_one('div>div[class = "inline-block ratings-imdb-rating"]>strong').text
        else:
            self.url = url_base + item_filme_html.a['href'] #contém a URL do filme.
            self.nota = item_filme_html.parent.select_one('strong').text
        self.__conecta()

    def __conecta(self):
        pagina_filme_html = obtem_html(self.url) #conecta-se a URL do filme para obter mais informações.
        self.__atribui_informacoes(pagina_filme_html)

    def __atribui_informacoes(self, pagina_filme_html: Tag):
        ##self.nota = pagina_filme_html.select_one(
        ##    'div>div>div>span[class ^= "AggregateRatingButton__RatingScore-sc"]').text
        self.nome = pagina_filme_html.select_one('div>div>h1[data-testid = "hero-title-block__title"]').text.replace('\n', '')
        self.__lista_diretores = pagina_filme_html.select_one(
            'div>div>ul[class="ipc-metadata-list ipc-metadata-list--dividers-all title-pc-list ipc-metadata-list--baseAlt"]') \
            .find('li') \
            .find('span', text={'Director','Directors'}) \
            .parent \
            .select(
            'div>ul>li>a[class="ipc-metadata-list-item__list-content-item ipc-metadata-list-item__list-content-item--link"]')
        self.diretores = ', '.join([it.text for it in self.__lista_diretores]).replace('\n', '')
        self.__lista_elenco_principal = pagina_filme_html.select(
            'section>div>div>div>a[data-testid="title-cast-item__actor"]')
        self.elenco_principal = ', '.join([it.text for it in self.__lista_elenco_principal]).replace('\n', '')
        self.__conecta_e_obtem_comentarios()
        print("[Instanciamento terminado] filme: " + self.nome + "; rate: " + self.nota)

    def __conecta_e_obtem_comentarios(self):
        self.__url_comentarios = self.url + "reviews?sort=userRating&dir=desc&ratingFilter=0"
        # Como a lista de comentários do filme está em outra página é necessário fazer uma nova conexão.
        # Para assim obter o primeiro comentário positivo, se existir.
        estrelas_html = obtem_html(self.__url_comentarios).select_one('div>span[class = "rating-other-user-rating"]>span')
        self.comentario_positivo = ''
        try:
            if (estrelas_html is not None and float(estrelas_html.text) >= 5.0):
                self.comentario_positivo = estrelas_html.parent.parent.parent.select_one(
                    'div>div[class = "text show-more__control"]').text.replace('\n', '')
        except ValueError:
            print("Não foi encontrada a avaliação em estrelas.")
        if (len(self.comentario_positivo) == 0):
            self.comentario_positivo = 'There is no positive comment.'

    def imprime(self):
        #Imprime o objeto, algo parecido com o famoso toString().
        return   "\"" + self.url + "\", " \
               + "\"" + self.nome + "\", " \
               + "\"" + self.nota + "\", " \
               + "\"" + self.diretores + "\", "\
               + "\"" + self.elenco_principal + "\", " \
               + "\"" + self.comentario_positivo + "\""

#---------------------------Aqui começa o programa--------------------------

tamanho_limit_lista = 10
url_base = 'https://www.imdb.com'
if obter_pior_verdadeiro:
    url = url_base + '/search/title/?user_rating=,10.0&sort=user_rating,asc'
else:
    url = url_base + '/chart/bottom?sort=ir,asc&mode=simple&page=1' #Obtem a pagina do Top 100 em ordem decrescente.

# Obtém o HTML da página que lista os piores filmes.
doc_html = obtem_html(url)
if obter_pior_verdadeiro:
    lista_filmes_html = doc_html.select('div[class = "lister-item mode-advanced"]')
else:
    lista_filmes_html = doc_html.select('table>tbody>tr>td[class = "titleColumn"]')

# Com a lista de elementos HTML de cada filme será possível fazer o mapeamento para uma lista de objetos Filme.
lista_filmes = [Filme(item_filme_html=item, url_base=url_base) for item in lista_filmes_html[:tamanho_limit_lista]]

print("---------------------------Top 10 piores filmes ---------------------------")
print("----------------------Ordenados do melhor para o pior----------------------")

lista_filmes.reverse() #Inverte a ordem da lista, pois é necessário ser na ordem do melhor para o pior.

csv = "URL, Nome, Nota, Diretor(es), Elenco Principal, Comentário Positivo\n"
csv = csv + "\n".join([it.imprime() for it in lista_filmes])

if obter_pior_verdadeiro:
    arquivo = open("piores_filmes.csv", "wt", encoding="utf8")
else:
    arquivo = open("piores_filmes_do_top_100.csv", "wt", encoding="utf8")
arquivo.writelines(csv)
arquivo.close()

print('['+csv.replace(', \"', ', [').replace('\",', '],')+']')

#---------------------------Aqui termina o programa--------------------------
