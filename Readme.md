# Avaliação técnica Overmind - Webcrawler

Esse projeto é o resultado de um desafio promovido na seleção da Overmind.
Foram desenvolvidas as funcionalidades de acordo com o que foi entendido nas instruções fornecidas.

# Instruções fornecidas

A prova consiste em construir um crawler em qualquer linguagem de programação, utilizando qualquer tecnologia nativa ou biblioteca aberta, que visite o site do IMDB (The Internet Movie Database) e procure pelos piores filmes da história ( https://www.imdb.com/chart/bottom ).
O resultado da execução do crawler deve ser:

- A Lista dos 10 filmes com pior nota no site. A lista pode ser exibida onde preferir (console, arquivo externo, página web), em ordem decrescente de melhor para pior nota, e deve conter as seguintes informações estruturadas:
- Nome do Filme (deve estar em inglês)
- Nota (com uma casa decimal)
- Diretor(es)
- Elenco principal
- Ao menos um comentário POSITIVO sobre o filme (comentário que deu uma nota >= 5)

Serão avaliados a completude dos requisitos, organização do código (independente da tecnologia utilizada), limpeza e facilidade de entendimento.

# O que é um Webcrawler?

Um Webcrawler é um agente que tem o intuito de encontrar, ler e indexar páginas de um website.
Existe uma diferença entre Crawler e Spider: spider é o que lê determinado site, enquanto que o Crawler é aquele que faz uso dos spiders.

O Google possui um WebCrawler chamado Googlebot, o qual é capaz de usar vários tipos de spiders para vários tipos de sites em sua base de dados.
A cada busca que é feita no Google, os resultados somente são possíveis devido a esse tipo de programa.

# O que é um Web scraping?

Web scrapping é o nome do processo que visa coletar os dados de forma estruturada da web, é bem utilizada em sites específicos 
e de maneira automatizada.

# Restrições a coleta de dados

Por mais que muitos dados na internet sejam públicos, os sites que os detém muitas vezes não permitem que sejam replicados fora do seu domínio.
Esse programa tem apenas o intuito avaliativo, os dados que foram obtidos não devem e não podem ser replicados em outro lugar sem a permissão
expressa do IMDb.

O programa ele faz apenas a leitura do código HTML do site IMDb, e faz apenas um acesso comum ao site, como se fosse um usuário.

# Instruções de execução

## Requerimentos

* Python versão 3.9 .
* Necessário ter o PIP mais atual no Python.
* Instalação das bibliotecas do Python: 
  * requests; 
  * beautifulsoup4.

## Observações

* O resultado final do programa é um arquivo CSV. Ele pode ser aberto no Libreoffice ou outro similar. O encoding obrigatoriamente
para a leitura deve ser o UTF-8, caso contrário haverá caracteres incorretos.
* O programa também imprime o resultado no terminal, mas não fica muito legível.
* Como na instrução foi dado a URL do Top 100 filmes do IMDb, e pediu os 10 piores filmes do site IMDb. Foram feito duas abordagens:
  * Modificando a variável `obter_pior_verdadeiro` para `True`, o programa realmente pegará os 10 piores filmes do site IMDb.
  * Modificando a variável `obter_pior_verdadeiro` para `False`, o programa pegará os 10 piores filmes do Top 100 do site IMDb.
